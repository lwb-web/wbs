# Technische Schnittstellenbeschreibung

[[_TOC_]]

# Einleitung
## Zielgruppe 

Dieses Dokument spezifiziert eine Schnittstelle in der Version 1.0 zwischen zwei Systemen. Es dient den beteiligten Systemen als Schnittstellenvertrag. 

Diese Schnittstellenbeschreibung wird sowohl von Architekten und Entwicklern auf der anbietenden Seite als auch von Architekten und Entwicklern auf der konsumierenden Seite als bindend berücksichtigt. 

## Zweck

Die Schnittstelle wurde für das Projekt "Umsetzung der Fördermittelrichtlinie in der Vermietung / Wohnberechtigungsschein" geschaffen. Sie dient zur Verknüfung der Vermietungsprozesse mit den Prozessen des Amt 50.52.

Die Beschreibung dieser Schnittstelle steht öffentlich zur Verfügung, um andere Wohnungsgesellschaften in die Lage zu versetzen am Verfahren teilzunehmen.

# Kommunikationsablauf

Der Datenaustausch läuft über eine sftp-Freigabe. Diese Freigabe enthält 2 Ordner. Ein Ordner dient als Ziel für die LWB aus der das Amt 50.52 (Empfänger) liest. Der andere Ordner dient als Ziel für das Amt 50.52 aus der die LWB (Empfänger) liest. Der Empfänger löscht nach der Verarbeitung die verarbeiteten Dateien aus dem jeweiligen Ordner.

Der Datentransfer wird vom jeweiligem Wohnungsunternehmen gestartet. Der Start des Transfers erfolgt LWB-seitig 20:30 Uhr.

Vermietung
```mermaid
sequenceDiagram
    participant A as LWB
    participant B as Amt 50.52
    A->>B: bf
    Note over A,B: wenn die genauen Wohnflächen<br> vorliegen
    A->>B: mve
    Note over A,B: Amt 50.52 sollte<br> innerhalb von 14 Tagen antworten
    Note over A,B: Anlagen:<br> Mietvertragentwurf, WBS
    A-->>B: mve
    Note over A,B: erneutes Versenden möglich,<br> da Interessent abspringen<br>kann, kein gesondertes Storno<br> notwendig
    B->>A: mvr (Ablehung)
    Note over A,B: Ablehung erfolgt mit<br> Begründung
    Note over A,B: Prozess endet damit
    B->>A: mvr (Zustimmung)
    A->>B: mv
    Note over A,B: Anlagen: Mietvertrag, WBS
    A->>B: ku
    Note over A,B: Kündigung wird erst bei Übergabe<br> der Wohnung versendet,<br> da eine Kündigung<br> storniert werden kann
    A->>B: mvs
    Note over A,B: Storno kann nur bei<br> vorhandenem Mietvertrag<br> übersendet werden
```
Mieterwechsel
```mermaid
sequenceDiagram
    participant A as LWB
    participant B as Amt 50.52
    A->>B: wve
    Note over A,B: Amt 50.52 sollte<br> innerhalb von 14 Tagen antworten
    Note over A,B: Anlagen:<br> Mietvertragentwurf, WBS
    A-->>B: wve
    Note over A,B: erneutes Versenden möglich,<br> da Interessent abspringen<br>kann, kein gesondertes Storno<br> notwendig
    B->>A: wvr (Ablehung)
    Note over A,B: Ablehung erfolgt mit<br> Begründung
    Note over A,B: Prozess endet damit
    B->>A: wvr (Zustimmung)
    A->>B: wv
    Note over A,B: Anlagen: Mietvertrag, WBS
    A->>B: wvs
    Note over A,B: Storno kann nur bei<br> vorhandenem Mietvertrag<br> übersendet werden
```
## Allgemeines

Die Schnittstelle ist datei-basiert. Die Dateien werden nur im xml-Format ausgetauscht. Zusätzliche Anlagen wie Mietverträge, deren Entwürfe und WBS-Nachweise werden im pdf-Format übermittelt.

## Sicherheit

Der Dateiaustausch wird über das [sftp-Protokoll](https://de.wikipedia.org/wiki/SSH_File_Transfer_Protocol) realisiert. Damit wird ein verschlüsselter und somit sicherer Dateitransfer gewährleistet.

In der Schnittstelle werden nur die für das Verfahren absolut notwendigen Daten übermittelt. Auf den Inhalt der Anlagen hat die Schnittstelle keinen Einfluss. 

## Dateinamen

Sämtliche Dateinamen sind wie folgt aufgebaut: `Präfix-TeilprozessNummer-MietobjektNummer.{xml|pdf}`

Die in der xml-Datei enthaltene Prozessnummer (process --> Teilprozessnummer) ist ebenso wie die Nummer des Mietobjekts, in allen Dateinamen enthalten. Grund dafür ist die einfachere Zuordnung zum Quell-Prozess und zum Mietobjekt im Fehlerfall.

Es wird dringend empfohlen die Namenskonventionen auch für die Anlagen einzuhalten, auch wenn durch direkte Referenzierung der Dateinamen in den xml-Dateien ein anderes Vorgehen möglich ist.

## Präfix

Der Präfix der xml-Datei entscheidet, welcher Vorgang darin beschrieben ist.

* [bf](bf-TeilprozessNummer-MietobjektNummer.xml): Bezugsfertigkeit (Vertragserfüllung Fördervertrag)
* [mve](mve-TeilprozessNummer-MietobjektNummer.xml): Mietvertrag-Entwurf
* [mvr](mvr-TeilprozessNummer-MietobjektNummer.xml): Mietvertrag-Rückantwort
* [mv](mv-TeilprozessNummer-MietobjektNummer.xml): Mietvertrag
* [mvs](mvs-TeilprozessNummer-MietobjektNummer.xml): Mietvertrag-Storno (Storno-Prozess)
* [ku](ku-TeilprozessNummer-MietobjektNummer.xml): Kündigung/Mieterwechsel (Mietvertrag ist gekündigt)
* at-mve-contract: Anlage Mietvertrag-Entwurf --> Mietvertrag-Entwurf
* at-mve-wbs: Anlage Mietvertrag-Entwurf --> Wohnberechtigungsschein
* at-mv-contract: Anlage Mietvertrag --> Mietvertrag
* at-mv-wbs: Anlage Mietvertrag --> Wohnberechtigungsschein

## Teilprozessnummer

Die Teilprozessnummer ist das eindeutige Merkmal für Teilprozesse. 

Der Prozess `Vermietung` besteht aus mehreren Teilen. Daher wird daher von Teilprozessen gesprochen. Das ist auch der Grund, weshalb sich die Teilprozessnummern unterscheiden in den xml-Dateien, obwohl es sich um den selben Vermietungsvorgang handelt.

# Fehlerbehandlung

Bei der Übermittlung fehlerhafter Dateien ist mit dem Amt 50.52 und deren IT-Dienstleister über den Einzelfall zu entscheiden.
